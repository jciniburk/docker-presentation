<html>
<style>
.content {
    max-width: 500px;
    margin: auto;
}
</style>
 <head>
   <title>PHP Test</title>
 </head>
 <body>
	<div class="content">
   <?php 
     echo '<p>Hello World</p>';
     date_default_timezone_set('Europe/Prague');
     echo 'Current PHP version: ' . phpversion() . ' in ' . date('Y-m-d H:i:s a', time()) . '<br>'; 
   ?>
   <img src="magic.gif"/>   
   </div>
 </body>
</html>
